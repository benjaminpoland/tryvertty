// Scrolling sidebar for your website
// Downloaded from Marcofolio.net
// Read the full article: http://www.marcofolio.net/webdesign/create_a_sticky_sidebar_or_box.html

window.onscroll = function()

{
	if( window.XMLHttpRequest ) {

		if (document.documentElement.scrollTop > 0 || self.pageYOffset > 0) {


      $('add-to-cart-button').style.position = 'fixed';


      $('add-to-cart-button').style.top = '0px';

		} else if (document.documentElement.scrollTop < 0 || self.pageYOffset < 0) {


      $('add-to-cart-button').style.position = 'absolute';


      $('add-to-cart-button').style.top = '0px';

		}

	}
}