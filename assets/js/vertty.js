

jQuery(document).ready(function() {

	//wait for the page to load before showing the carousel
	//the carousel looks abnormal until fully loaded
	jQuery("#towel-carousel").fadeIn("slow");

  jQuery('ul').roundabout({
	 btnPrev: '#btn-prev-towel',
     btnNext: '#btn-next-towel',
	 minOpacity: 1,
	 minScale: .05,
	 duration: 150
  });

  jQuery(".roundabout-moveable-item").click(function() {
  	
  });
  
  var randPhrase = getRandomPhrase();
  jQuery('#quote-text').append('\"'+randPhrase+'\"');

  function getRandomPhrase() {
    var numRand = Math.floor(Math.random()*5);
	var phrase  = '';
	
	if (numRand == 0) {
	  phrase = "All I’m thinking about is the glory days, when I can forget the buildings and the highways, the meetings and the bad ways. I can stop that clock on my way to the beach.";
	} else if (numRand == 1) {
	  phrase = "Remove the tragic from my life, I must. No more shoving, no more crowds, how does it feel? Clothes, Shoes, Mind. All off. How does it feel, now?";
	} else if (numRand == 2) {
	  phrase = "I leave the city, but she comes along. It’s part of who I am. The tone in my eyes, the outline of my hand. I stand out from the conventional, for who I am always stays with me.";
	} else if (numRand == 3) {
	  phrase = "Run for it, the great escape. To where the sun darkens the bad; to where the sun lightens the good. Do not call it rest, because it isn’t.";
	} else if (numRand == 4) {
	  phrase = "Even if everything does change, I’ll never do.";
	}
	
	return phrase;
  }
  
  jQuery(function getInstagramPhotos(start, end) {
    jQuery.ajax({
    	type: "GET",
        dataType: "jsonp",
        cache: false,
        url: "https://api.instagram.com/v1/users/2514893/media/recent/?access_token=2514893.5b9e1e6.1360fe167c184623826ecdf0c2185861",
        success: function(data) {
            for (var i = 0; i < 5; i++) {
				if ((i == 0) || ((i%4) != 0)) {
				  jQuery("#photo-line").append("<div class='instagram-placeholder'><a target='_blank' href='" + data.data[i].link +"'><img class='instagram-image' width='184' src='" + data.data[i].images.low_resolution.url +"' /></a></div>");   
				} else {
				  jQuery("#photo-line").append("<div class='instagram-placeholder-right'><a target='_blank' href='" + data.data[i].link +"'><img class='instagram-image' width='184' src='" + data.data[i].images.low_resolution.url +"' /></a></div>");
				}
			}                            
        }
    });
  });
  
  jQuery("#menu-items").each(function() {
		jQuery(this).find('li').each(function(index) {
			jQuery(this).click(function () {
				jQuery("#menu").empty();
				jQuery("#menu").append(getContent(jQuery(this).text()));
        if (jQuery(".item").hasClass("selected")) {
          jQuery(".item").removeClass("selected");
        }
        jQuery(this).addClass("selected");
			});
			jQuery(this).click(function () {
				jQuery("#pages").each(function() {
					jQuery(this).find('.page').each(function(index) {
						jQuery("#menu-bottom-nav-pages").empty();
						jQuery("#menu-bottom-nav-pages").append(generatePagination(jQuery(".page").size()));
            jQuery("#menu-bottom-nav-pages li").first().addClass("selected");
					});
				});
				jQuery("#menu-bottom-nav-pages").each(function() {
					jQuery(this).find('li').each(function(index) {
						jQuery(this).click(function () {
							var currDiv = "#page"+jQuery(this).text();
							jQuery('#pages > :not(currDiv)').hide();
							jQuery("#page"+jQuery(this).text()).show();
              if (jQuery(".page-nav-button").hasClass("selected")) {
                jQuery(".page-nav-button").removeClass("selected");
              }
              jQuery(this).addClass("selected");
						});
					});
				});			
			});
		});
  });

  jQuery("#bottom-nav-left-arrow").click (function() {
    jQuery("#menu-bottom-nav-pages").find("li").each(function(index) {
      if (jQuery(this).hasClass("selected")) {
        jQuery(this).prev().trigger("click");
        return false;
      }
    });     
  });

  jQuery("#bottom-nav-right-arrow").click (function() {
    jQuery("#menu-bottom-nav-pages").find("li").each(function(index) {
      if (jQuery(this).hasClass("selected")) {
        jQuery(this).next().trigger("click");
        return false;
      }
    });
  });
  
  var getContent = function(content) { 
    if (content == 'ESSENCE') {
		var string = '<div class="span16" id="pages"> \
      <div class="page" id="page1"> \
        <div id="left-column"> \
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit, leo sit amet ullamcorper mollis, ipsum velit mattis lectus, eu pulvinar ligula lorem lobortis purus. Mauris turpis lectus, eleifend pharetra luctus nec, lacinia a quam. In hac habitasse platea dictumst. In nec faucibus mauris. Quisque neque lectus, blandit ut hendrerit quis, eleifend eget purus. Curabitur id aliquet urna. In a odio sed lacus tincidunt viverra et vel risus.</p> \
        </div> \
        <div id="right-column"></div> \
      </div> \
     <div class="page" id="page2" style="display: none;"> \
        <div id="left-column"> \
        <p>Sed egestas sem quis tellus eleifend volutpat. Duis posuere porttitor sapien, eu convallis lacus sodales vitae. Cras at felis at sapien luctus elementum eget et mi. Nullam in neque nulla, et convallis urna. Quisque laoreet diam sit amet diam bibendum at vulputate diam euismod. Nullam quis feugiat lorem. Proin id eros at dolor sagittis fringilla. Praesent hendrerit eleifend pharetra. Maecenas odio odio, malesuada nec tincidunt vel, condimentum eu nunc. Vivamus pretium suscipit massa, eu cursus risus commodo id. Mauris lorem libero, consequat at consectetur at, accumsan a nulla.</p> \
        </div> \
        <div id="right-column"></div> \
      </div> \
      <div class="page" id="page3" style="display: none;"> \
        <div id="left-column"> \
        <p>Donec quam sapien, malesuada vitae volutpat sed, lacinia eu erat. Curabitur commodo euismod elit sit amet lacinia. Phasellus lobortis, lorem ut elementum blandit, dui tortor lobortis elit, et pharetra ipsum diam at dolor. Sed id mattis tellus. Proin rutrum sollicitudin velit id sollicitudin. Praesent lobortis ornare vehicula. Maecenas tincidunt pulvinar suscipit. Integer odio neque, bibendum et porta sed, fringilla id massa. Integer placerat, odio varius cursus vehicula, massa enim facilisis sem, eu rutrum libero lorem vel nisl. Nullam sed massa lacus, sit amet porta ante. Vivamus hendrerit urna vitae nulla porta nec rhoncus ligula ultrices. Sed eget felis imperdiet tortor facilisis hendrerit. Nullam quis sem aliquet magna condimentum sodales. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non elit commodo eros mattis consectetur placerat sit amet dolor.</p> \
        </div> \
        <div id="right-column"></div> \
      </div> \
        </div> \
        <div id="menu-mask"><img src="assets/img/menu_mask.png" /></div> \
        <div id="menu-background"><img src="assets/img/menu_temp_bg.png" /></div>';
		return string;
	} else if (content == 'TEAM') {
		var string = '<div class="span16" id="pages"> \
		  <div class="page" id="page1"> \
			  <div id="left-column"> \
			  <p>TEAM Page 1</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
		  <div class="page" id="page2" style="display: none;"> \
			  <div id="left-column"> \
			  <p>TEAM Page 2</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
        </div>';
		return string;
	} else if (content == 'EVENTS') {
		var string = '<div class="span16" id="pages"> \
		  <div class="page" id="page1"> \
			  <div id="left-column"> \
			  <p>EVENTS Page 1</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
        </div>';
		return string;
	} else if (content == 'CONTACTS') {
		var string = '<div class="span16" id="pages"> \
		  <div class="page" id="page1"> \
			  <div id="left-column"> \
			  <p>CONTACTS Page 1</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
		 <div class="page" id="page2" style="display: none;"> \
			  <div id="left-column"> \
			  <p>CONTACTS Page 2</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
		  <div class="page" id="page3" style="display: none;"> \
			  <div id="left-column"> \
			  <p>CONTACTS Page 3</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
		  <div class="page" id="page4" style="display: none;"> \
			  <div id="left-column"> \
			  <p>CONTACTS Page 4</p> \
			  </div> \
			  <div id="right-column"></div> \
		  </div> \
        </div>';
		return string;
	} 
  };
  
  var generatePagination = function(numPages) {
	var string = '';
	for (var i = 1; i <= numPages; i++) {
		if (numPages == 1) {
			string = '<li class="page-nav-button">1</li>';
		} else {
			string = string + '<li class="page-nav-button">' + i + '</li>';
		}
	}
	return string;
  };
  
  //forcing the first item to click itself
  //this makes the menu pagination immediately functional
  jQuery(".item1").trigger("click");

});